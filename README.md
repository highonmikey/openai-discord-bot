# OpenAI Discord Bot

This Discord bot uses the OpenAI API to generate text, code, and images for all users in a Discord channel. It
is written for Python 3.11 and utilizes the [discord.py](https://github.com/Rapptz/discord.py) and
[openai-python](https://github.com/openai/openai-python) libraries.

## Discord Application and Bot Setup

### Create a Discord Application and Bot

1. Create a [Discord Application](https://discord.com/developers/applications)
2. Add a Bot to the Application
3. Copy the Bot token
4. Optionally save the token in a secure credential store or password manager for later retrieval

### Create an OpenAI API Key

You will need an [API key](https://platform.openai.com/account/api-keys) to use the OpenAI API, which requires a paid
account. It's highly recommended to set a [Hard Limit](https://platform.openai.com/account/billing/limits) on the usage.

Optionally, you can save the API key in a secure credential store or password manager for later retrieval.

### Add the Bot to your Discord Server

1. In your Discord application, Under **OAuth2** -> **URL Generator**, select the `bot` scope
2. Copy the generated URL and open it in a browser to add the bot to your server

#### (Optional) Adding permissions to the log channel

If the `DISCORD_LOG_CHANNEL_ID` environment variable was provided, the bot will send status logs to the channel. This
channel will need to set the bot's `Send Messages` permission.

1. In your server, right-click on the channel you want the bot to send status logs to and select `Edit Channel`
2. Under **Permissions**, select add your bot and check the following permissions:
    - `Send Messages`

#### Controlling access to slash commands

Finer control over slash command access can be changed under `Server Settings -> Integrations -> [Your Bot] -> Manage`.

## Running the Bot

A Docker Compose definition is provided for convenience. Alternatively, you can run the bot directly with Python, which
is useful for development.

### 1. Docker Compose

```bash
# Copy .env.dev then edit .env to add Discord token and OpenAI API key
cp .env.dev .env
docker-compose up -d
```

### 2. Plain Python (Development)

Requires Python 3.11 or higher.

```bash
pip install -e .[dev]

# Copy .env.dev then edit .env to add Discord token and OpenAI API key
cp .env.dev .env

openai-discord-bot
```

## Environment variables

| Variable                    | Required | Description                                          |
|-----------------------------|----------|------------------------------------------------------|
| `DISCORD_LOG_CHANNEL_ID`    | No       | Channel to send basic bot status messages            |
| `DISCORD_PRIMARY_SERVER_ID` | No       | Updates the command tree immediately for this server |
| `DISCORD_TOKEN`             | **Yes**  |                                                      |
| `OPENAI_API_KEY`            | **Yes**  |                                                      |
| `OPENAI_ORGANIZATION`       | **Yes**  |                                                      |
