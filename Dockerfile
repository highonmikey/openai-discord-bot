FROM python:3.11-alpine
WORKDIR /bot
COPY . .
RUN pip install .
CMD ["openai-discord-bot"]
