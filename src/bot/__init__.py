"""Bot package."""
from console import console

from bot.bot import run_discord_bot


def run():
    """Run the bot."""
    console.log("Bot starting...")
    run_discord_bot()
