"""Discord bot for OpenAI API."""

from os import environ
from typing import Literal

import discord
import dotenv
import openai

from discord import app_commands

from bot import handlers, formatters
from bot.code import Languages
from console import console

# Generic setup
dotenv.load_dotenv()

# Discord setup
discord_log_channel_id = environ.get("DISCORD_LOG_CHANNEL_ID")
primary_discord_server_id = environ.get("DISCORD_PRIMARY_SERVER_ID")
intents = discord.Intents.default()
client = discord.Client(intents=intents)
tree = app_commands.CommandTree(client)

# OpenAI setup
openai.organization = environ.get("OPENAI_ORGANIZATION")
openai.api_key = environ.get("OPENAI_API_KEY")


@client.event
async def on_ready():
    """Initialize the bot and sync commands."""
    if primary_discord_server_id:
        primary_discord_server = discord.Object(id=primary_discord_server_id)
        tree.copy_global_to(guild=primary_discord_server)
        await tree.sync(guild=primary_discord_server)
    if discord_log_channel_id:
        channel = client.get_channel(int(discord_log_channel_id))
        try:
            await channel.send("OpenAI Discord bot initialized and ready")
        except discord.Forbidden:
            console.log("[red]ERROR[/red]: Bot does not have permission to send messages to the log channel")
    console.log("OpenAI Discord bot initialized and ready")


@tree.command(name="wtf")
@app_commands.describe(prompt="The prompt sent to OpenAI for randomness")
async def wtf(interaction: discord.Interaction, *, prompt: str):
    """Generate random, mostly nonsense, text from a prompt."""
    console.log(f"Received wtf request: {prompt}")
    await interaction.response.defer(ephemeral=False)
    response = await handlers.text_completion(prompt, temperature=1.5, max_tokens=400)
    console.print(response)
    messages = formatters.split_response(response)
    for message in messages:
        try:
            await interaction.followup.send(message)
        except Exception as e:
            console.log(e)
            await interaction.followup.send("Something went wrong.")


@tree.command(name="chat")
@app_commands.describe(prompt="The prompt sent to OpenAI for text generation")
async def chat(interaction: discord.Interaction, *, prompt: str):
    """Generate text from a prompt."""
    console.log(f"Received chat request: {prompt}")
    await interaction.response.defer(ephemeral=False)

    response = await handlers.text_completion(prompt)
    messages = formatters.split_response(response)

    for message in messages:
        try:
            await interaction.followup.send(message)
        except Exception as e:
            console.log(e)
            await interaction.followup.send("Something went wrong.")


@tree.command(name="code")
@app_commands.describe(language="The programming language for code completion")
@app_commands.describe(prompt="The prompt sent to OpenAI for code completion")
async def code(interaction: discord.Interaction, language: Languages, prompt: str):
    """Generate code from a prompt."""
    console.log(f"Received code completion request: ({language}) {prompt}")
    await interaction.response.defer(ephemeral=False)

    language_name = language.value[0]
    comment_syntax = language.value[1]
    formatted_prompt = f"{comment_syntax} {language_name} language\n{comment_syntax} {prompt}\n\n"

    response = await handlers.code_completion(formatted_prompt)
    wrapped_code = formatters.wrap_code(response, language_name)
    try:
        if len(wrapped_code) >= 2000:
            code_file = handlers.save_code(response, formatted_prompt, language.value[2])
            file = discord.File(code_file, filename="code.txt")
            file.description = f"({language}) {prompt}"
            await interaction.followup.send(file=file)
        else:
            await interaction.followup.send(wrapped_code)
    except Exception as e:
        console.log(e)
        await interaction.followup.send("Something went wrong.")


@tree.command(name="image")
@app_commands.describe(prompt="The prompt sent to OpenAI for image generation")
@app_commands.describe(size="Size of the image (256x256, 512x512, or 1024x1024)")
async def image(
    interaction: discord.Interaction, prompt: str, size: Literal["256x256", "512x512", "1024x1024"] = "512x512"
):
    """Generate an image from a prompt."""
    console.log(f"Received image generation request: {prompt}")
    await interaction.response.defer(ephemeral=False)

    try:
        response = await handlers.image_request(prompt, size)
        file = discord.File(handlers.save_image(response, prompt), filename="image.png")
        embed = discord.Embed()
        embed.description = prompt
        embed.set_image(url="attachment://image.png")
        await interaction.followup.send(file=file, embed=embed)
    except Exception as e:
        console.log(e)
        await interaction.followup.send("Something went wrong.")


def run_discord_bot():
    """Run the Discord bot."""
    client.run(environ.get("DISCORD_TOKEN"))
