"""Code completion for programming languages."""

import enum


class Languages(enum.Enum):
    """Enum of supported programming languages for code completion."""

    assembly = ("x86asm", ";", ".asm")
    bash = ("bash", "#", ".sh")
    c = ("c", "//", ".c")
    cpp = ("cpp", "//", ".cpp")
    css = ("css", "//", ".css")
    c_sharp = ("csharp", "//", ".cs")
    erlang = ("erlang", "%", ".erl")
    go = ("go", "//", ".go")
    groovy = ("groovy", "//", ".groovy")
    html = ("html", "", ".html")
    java = ("java", "//", ".java")
    javascript = ("javascript", "//", ".js")
    kotlin = ("kotlin", "//", ".kt")
    lua = ("lua", "--", ".lua")
    matlab = ("matlab", "%", ".m")
    objective_c = ("objectivec", "//", ".m")
    perl = ("perl", "#", ".pl")
    php = ("php", "#", ".php")
    powershell = ("powershell", "#", ".ps1")
    python = ("python", "#", ".py")
    r = ("r", "#", ".r")
    ruby = ("ruby", "#", ".rb")
    rust = ("rust", "//", ".rs")
    scala = ("scala", "//", ".scala")
    swift = ("swift", "//", ".swift")
