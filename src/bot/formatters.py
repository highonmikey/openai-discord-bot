"""Response formatting for Discord interactions."""


def cycle_delimiters(delimiter: str | None = None) -> str:
    """Cycles through delimiters for splitting responses."""
    if delimiter == "\n\n":
        return "\n"
    if delimiter == "\n":
        return " "

    return "\n\n"


def split_response(
    response: str, delimiter: str = "\n\n", result_list: list[str] = None, max_length: int | None = 1900
) -> list:
    """Split a response into a list of strings."""
    st = [s for s in response.split(delimiter) if s != ""]
    if not result_list:
        result_list = [""]

    for t in st:
        if len(result_list[-1]) + len(t) < max_length:
            result_list[-1] += t + delimiter
        else:
            result_list.append("")
            split_response(t, delimiter=cycle_delimiters(delimiter), result_list=result_list, max_length=max_length)
    return [r for r in result_list if r != ""]


def wrap_code(code: str, language: str = ""):
    """Wrap text/code in a Markdown code block."""
    return f"```{language}\n{code}\n```"
