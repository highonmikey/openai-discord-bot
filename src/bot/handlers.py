"""Handlers for the bot."""

from hashlib import sha1
from pathlib import Path
from typing import Literal

import openai
import requests

from console import console


async def image_request(prompt: str, size: str = "256x256"):
    """Request an image from OpenAI."""
    try:
        response = openai.Image.create(
            prompt=prompt,
            n=1,
            size=size,
        )
    except Exception as e:
        console.log(e)
        return "Something went wrong."

    return response["data"][0]["url"]


async def text_completion(prompt, temperature: float = 1, max_tokens: int = 1900):
    """Request text completion from OpenAI."""
    if not prompt.endswith("\n\n"):
        prompt += "\n\n"

    try:
        response = openai.Completion.create(
            model="text-davinci-003",
            prompt=prompt,
            max_tokens=max_tokens,
            temperature=temperature,
        )
    except Exception as e:
        console.log(e)
        return "Something went wrong."

    return response["choices"][0]["text"]


async def code_completion(prompt):
    """Request code completion from OpenAI."""
    if not prompt.endswith("\n\n"):
        prompt += "\n\n"

    try:
        response = openai.Completion.create(
            model="code-davinci-002",
            prompt=prompt,
            max_tokens=512,
            temperature=0,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0,
        )
    except Exception as e:
        console.log(e)
        return "Something went wrong."

    return response["choices"][0]["text"]


def save_file(base_path: Literal["images", "code"], prompt: str, file_ext: str, text: str = "", url: str = "") -> Path:
    """Save a file to disk."""
    file_hash = sha1(prompt.encode("utf-8"), usedforsecurity=False).hexdigest()
    if not file_ext.startswith("."):
        file_ext = f".{file_ext}"

    file_base_path = Path(f"{base_path}/")
    if not file_base_path.exists():
        file_base_path.mkdir()

    file = file_base_path.joinpath(f"{file_hash}{file_ext}")
    desc_file = file_base_path.joinpath(f"{file_hash}.txt")

    if url:
        res = requests.get(url, timeout=10)
        file.write_bytes(res.content)
    elif text:
        file.write_text(text, encoding="utf-8")
    else:
        raise ValueError("Must provide either text or url")

    desc_file.write_text(prompt, encoding="utf-8")

    return file


def save_image(url: str, prompt: str) -> Path:
    """Save an image to disk."""
    return save_file("images", prompt, url=url, file_ext=".png")


def save_code(code: str, prompt: str, file_ext: str) -> Path:
    """Save code to disk."""
    return save_file("code", prompt, text=code, file_ext=file_ext)
